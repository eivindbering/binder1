binder1
=======

.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/eivindbering%2Fbinder1/HEAD?labpath=notebooks/sample.ipynb
   :alt: Launch on Binder

binder test



Credits
-------

This project was created with `Cookiecutter`_ and the `pyvista/cookiecutter-pyvista-binder`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`pyvista/cookiecutter-pyvista-binder`: https://github.com/pyvista/cookiecutter-pyvista-binder
